package com.bolsadeideas.springboot.error.app.services;

import com.bolsadeideas.springboot.error.app.domains.Usuario;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioServiceImpl implements UsuarioService{

    private List<Usuario> lista;

    public UsuarioServiceImpl(){
        this.lista = new ArrayList<>();
        this.lista.add(new Usuario(1,"Fermin","Flores"));
        this.lista.add(new Usuario(2,"Tommy","Jasso"));
        this.lista.add(new Usuario(3,"Sophia","Lauren"));
        this.lista.add(new Usuario(4,"Edgar","Loeffelman"));
        this.lista.add(new Usuario(5,"Dante","Sparda"));
        this.lista.add(new Usuario(6,"Marcus","Finix"));
        this.lista.add(new Usuario(7,"Leonardo","Davinci"));
    }

    @Override
    public List<Usuario> listar() {
        return lista;
    }

    @Override
    public Usuario obtenerPorId(Integer id) {
        Usuario resultado = null;
        for (Usuario u: this.lista){
            if (u.getId().equals(id)){
                resultado = u;
                break;
            }
        }
        return resultado;
    }

    @Override
    public Optional<Usuario> obtenerPorIdOptional(Integer id) {
        Usuario usuario = this.obtenerPorId( id );
        return Optional.ofNullable(usuario);
    }
}
